import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by heatherhua on 3/23/17.
 */
public class Tests {

    String CSV_FILE = "dataset.csv";
    String EMPTY_CSV_FILE = "empty.csv";
    ArrayList<String> pathA;
    ArrayList<String> pathB;

    @Before
    public void setUp() {
        pathA = new ArrayList<>(3);
        pathA.add("/");
        pathA.add("subscribers");
        pathA.add("/");

        pathB = new ArrayList<>(3);
        pathB.add("/");
        pathB.add("subscribers");
        pathB.add("/");
    }

    @Test
    public void testCountIncreaseHashMap(){
        HashMap<ArrayList<String>, Integer> map = new HashMap<ArrayList<String>, Integer>();
        map.put(pathA, 1);
        map.put(pathB, map.get(pathB)+1);
        assert map.get(pathB) == 2;
    }

    @Test
    public void testReadCSVFile(){
        DatasetReader reader = new DatasetReader();
        HashMap<String, ArrayList<String>> map = reader.getUserPathsFromFile(CSV_FILE);
        assert map.containsKey("u1") == true;
        assert map.containsKey("u2") == true;
        assert map.containsKey("u3") == true;
        assert map.containsKey("nothere") == false;
    }

    @Test
    public void testReadEmptyCSVFile(){
        DatasetReader reader = new DatasetReader();
        HashMap<String, ArrayList<String>> map = reader.getUserPathsFromFile(EMPTY_CSV_FILE);
        assert map.size() == 0;
    }

    @Test
    public void testUniquePaths(){
        ArrayList<String> pathHistory = new ArrayList<>();
        pathHistory.add("/");
        pathHistory.add("subscribers");
        pathHistory.add("filter");
        pathHistory.add("export");
        pathHistory.add("filter");
        pathHistory.add("cancel");

        HashMap<String, ArrayList<String>> userPath = new HashMap<>();
        userPath.put("testUser", pathHistory);
        HashMap<List<String>, Integer> pathCount = Main.getPathCountMap(userPath, 3);
        assert pathCount.size() == 4;
    }
}
