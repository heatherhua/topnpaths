import java.util.*;
import java.util.logging.Logger;

/**
 * Created by heatherhua on 3/23/17.
 */
public class Main {
    private static Logger logger = Logger.getLogger(Main.class.getSimpleName());

    public static HashMap<List<String>, Integer> getPathCountMap(HashMap<String, ArrayList<String>> userPaths, int seqNum){
        HashMap<List<String>, Integer> pathCountMap = new HashMap<>();
        userPaths.forEach((k,v) -> {
            logger.fine(k + v.toString());

            int startWindow = 0;
            int endWindow = startWindow + seqNum;

            // Slide window over until we've added all unique paths to path count map.
            while(endWindow <= v.size()){
                List<String> path = v.subList(startWindow++, endWindow++);
                // Add unique path to map and initiate count to 1
                if(!pathCountMap.containsKey(path)) {
                    pathCountMap.put(path, 1);
                }else {
                    // path already exists, so increment path count
                    pathCountMap.put(path, pathCountMap.get(path)+1);
                }
            }
        });
        return pathCountMap;
    }

    public static void main(String[] args){
        // This is where we store the unique paths and the visit count
        HashMap<List<String>, Integer> pathCountMap;

        if(args.length < 2){
            System.out.println("Not enough arguments supplied. Please provide a file and number of top paths");
            return;
        }

        // First argument is filename, second argument is top N
        String csvFile = args[0];
        Integer topN = Integer.parseInt(args[1]);

        // Easter egg!
        // Just in case we want a larger path context
        Integer seqNum = args.length > 2 ? Integer.parseInt(args[2]) : 3;

        // Use the DatasetReader to separate users and their full path histories
        // Takes O(N) time
        // Then go through each user's path with a 3-page window slider
        // For each window, this is a path to keep visit count
        // Going through all the paths takes another O(N)
        HashMap<String, ArrayList<String>> userPaths = new DatasetReader().getUserPathsFromFile(csvFile);
        pathCountMap = getPathCountMap(userPaths, seqNum);

        // Sorting the list of paths by count
        // Takes O(NlogN)
        List<Map.Entry<List<String>, Integer>> topPaths = new ArrayList(pathCountMap.entrySet());
        Collections.sort(topPaths, (path1, path2) -> path1.getValue().compareTo(path2.getValue()) * -1);

        // Start from the top of the list and return N amount of paths
        for(int i = 0; i < topN; i++){
            if(i == topPaths.size()){
                // Ran through all paths before we hit N
                System.out.println("Returned all possible paths, only had " + i + " paths, not " + topN);
                break;
            }
            Map.Entry<List<String>, Integer> entry = topPaths.get(i);
            logger.fine("Top Path: " + entry.getKey().toString());
            logger.fine("Top Count: " + entry.getValue());
            System.out.println(i+1 + " Path: " + entry.getKey().toString() + " with count: " + entry.getValue());
        }
    }
}
