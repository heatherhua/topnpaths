import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created by heatherhua on 3/23/17.
 */
public class DatasetReader {

    // Separate users and their path history and store into hash map
    // This hash map will be returned to be processed in Main
    private HashMap<String, ArrayList<String>> userPathMap = new HashMap<String, ArrayList<String>>();
    private Logger logger = Logger.getLogger(DatasetReader.class.getSimpleName());

    public HashMap<String, ArrayList<String>> getUserPathsFromFile(String fileName) {
        BufferedReader bufferedReader;
        String line;

        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            // O(N) time to read through the dataset
            while((line = bufferedReader.readLine()) != null){
                String[] columns = line.split(",");
                // Assume first column is user and second column is page
                String user = columns[0];
                String page = columns[1];

                // Add a new user and initiate building their path history
                if(!userPathMap.containsKey(user)){
                    userPathMap.put(user, new ArrayList<>());
                    userPathMap.get(user).add(page);

                }else {
                    // If user exists, append to the user's path
                    userPathMap.get(user).add(page);
                }
            }

        } catch (FileNotFoundException ex){
            logger.severe(ex.getMessage());
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }

        return userPathMap;
    }
}
