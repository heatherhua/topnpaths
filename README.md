# README #

### What is this repository for? ###

This project will take in a CSV file containing 
access log data and top N paths to return. 

### How do I get set up? ###

* Make sure Java 1.8 is installed
* Create CSV file with format {user},{page} on each line
* top-paths-1.0.jar is already available in the target directory, 
but if you want to recreate a jar file, make sure Maven is installed and run this: 

`mvn clean package`

### How to test ###
Navigate to TopPaths directory

`mvn test`

### How to run in command line? ###

`java -jar top-paths-1.0.jar {CSV file} {N}`

For example:
1) Navigate to TopPaths directory
2) Type `java -jar target/top-paths-1.0.jar dataset.csv 3`
3) Returns top 3 paths within the dataset.csv file

### Bonus ###

If the path definition changes from 3 sequential pages to 4 or something else, 
just add another argument. For example: 

```
cd TopPaths
java -jar target/top-paths-1.0.jar dataset.csv 3 5
```

This returns the top 3 paths of dataset.csv where a path is 5 sequential page visits